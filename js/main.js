var myApp = {
    'always': function () { console.log('always'); },
    'done': function () { console.log('done'); },
    'fail': function () { console.log('fail'); },
    'then': function () { console.log('then'); },
};
$(function() {
    $('.view-source').find('h3').click(function(evt) {
        console.log(arguments);
        var target = evt.target || evt.srcElement;
        $(target).parent().find('code').toggle();
    });
    $('#theButton').click(function() {
        $.ajax('ajax.php')
            .done(myApp.done)
            .fail(myApp.fail)
            .then(myApp.then)
            .always(myApp.always);
    });
});
