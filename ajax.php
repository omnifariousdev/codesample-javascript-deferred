<?php
/** randomly succeed/fail to respond to an AJAX request */
sleep(4);
$succeed = array_rand(array(true, false));
if ($succeed) {
    $thing = new stdClass;
    $thing->status = 200;
    $thing->data = [];
    $thing->message = 'success!';
    header('content-type: application/json');
    echo json_encode($thing);
} else {
    // let's have some fun & create an infinite number of objects until PHP runs out of memory
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    echo 'darn';
}
