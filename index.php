<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Quick and dirty Deferred example</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <style>
            /* yes, inline styling is icky, but thank you for reading my source code. -- Daniel */
            .view-source {
                background-color: rgba(0,0,0,.2);
            }
            body {
                background-color: #abcdef;
            }
            section {
                background-color: #fff;
                margin: 20px;
                padding: 10px;
            }
            section>div {
                width: 400px;
                background-color: rgba(0,0,0,.05);
                outline: 3px solid black;
                margin: 10px auto;
            }
            ul {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            div>ul * {
                padding: 0 5px;
                margin: 0 5px;
                outline: 1px solid black;
                background-color: rgba(0,0,0,.2);
            }
            .view-source code {
                font-family: monospace;
                white-space: pre;
                margin: 1em 0;
                transition: width 2s, height 2s, transform 2s;
                display: none;
            }
            .userFeedback {
                background-color: blue
                text-align: center;
                color: white;
                background-color: rgba(0,0,255, .2);
            }
            #theButton {
                background-color: red;
                color: white;
                width: 200px;
                height: 100px;
            }
        </style>
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <h1>Quick and dirty Deferred example</h1>

        <section id="suboptimal-example">
            <h2>Using Deferred stuff via jQuery (incomplete example) </h2>
            <p>TODO: finish writing an example</p>
            <div>
            <p class="userFeedback">Click Stuff below</p>
            </div>
            <a id="theButton">Click this Button!</a>
            <blockquote class="view-source">
                <h3>Click to show/hide JavaScript Source</h3>
                <code><?readfile('js/main.js');?></code>
            </blockquote>
        </section>


        <footer>
        <cite>&copy; <?=date('Y', filemtime(__FILE__));?> Daniel J. Post</cite>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
        <script src="js/main.js"></script>

    </body>
</html>
